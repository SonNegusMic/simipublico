function getSelectSelectedNew(tabla, campoId, campo, parametro, campoEstado, estado, complemento, debug, dataType, elemento, titulo, valor, selected) {
    // valor=(valor == "")?0:valor;
    $.ajax({
        url: 'https://www.simiinmobiliarias.com/models/funciones.php?data=' + 2,
        type: 'POST',
        dataType: dataType,
        data: {
            tabla: tabla,
            campoId: campoId,
            campo: campo,
            parametro: parametro,
            campoEstado: campoEstado,
            Estado: estado,
            complemento: complemento,
            debug: debug
        },
        success: function(data) {
            var html = '';
            if (debug == 1 || debug == 2) {
                console.log(data);
            }
            if (data != "") {
                html += "<option value='" + valor + "'>" + titulo + "</option>";
                var i;
                for (i in data) {
                    html += "<option value='" + data[i].Codigo + "'>" + data[i].Descrip + "</option>";
                }
            } else {
                html += "<option value='" + valor + "'>Sin " + titulo + "</option>";
                $(elemento).prop("disabled", true);
            }
            $(elemento).html(html);
            if (selected != "") {
                $(elemento).val(selected);
            }
            // $(elemento).select2();
        }
    });
}

function explode(object, text) {
    var mystr = object;
    var arreglo = [];
    //Splitting it with : as the separator
    var myarr = mystr.split(text);
    //Then read the values from the array where 0 is the first
    for (var i = 0; i < myarr.length; i++) {
        arreglo.push(myarr[i])
    }
    return arreglo;
}

function formatNumber(input, errorElement, errorText) {
    var num = $(input).val().replace(/\./g, '');
    if (!isNaN(num)) {
        num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g, '$1.');
        num = num.split('').reverse().join('').replace(/^[\.]/, '');
        $(input).val(num);
        // $(errorElement).hide();
    } else {
        //$(errorElement).html(errorText);
        //$(errorElement).show();
        $(input).val($(input).val().replace(/[^\d\.]*/g, ''));
    }
}

function ajaxSimple(uri, method, dataTypes, data, debug) {
    return $.ajax({
        url: uri,
        type: method,
        dataType: dataTypes,
        data: data,
    });
}

function ajaxcache(uri, method, dataTypes, data, debug) {
    return $.ajax({
        url: uri,
        type: method,
        dataType: dataTypes,
        data: data,
        async: true,
        cache: false
    });
}

function ajaxSofisma(uri, method, dataTypes, data, idsofis, presofisma, block) {
    return $.ajax({
        url: uri,
        type: method,
        dataType: dataTypes,
        data: data,
        beforeSend: function() {
            idsofis.html(presofisma);
            if (block === 1) {
                idsofis.prop('disabled', true);
            }
        }
    });
}
function validateParsley(form) {
    $.listen('parsley:field:error', function(ParsleyField) {
        ParsleyField.$element.closest('.input-field').removeClass('has-success');
        ParsleyField.$element.closest('.input-field').addClass('has-error');
    });
    $.listen('parsley:field:success', function(ParsleyField) {
        ParsleyField.$element.closest('.input-field').removeClass('has-error');
        // ParsleyField.$element.closest('.input-field').addClass('has-success');
        ParsleyField.$element.closest('.input-field').addClass('has-success');
    });
}