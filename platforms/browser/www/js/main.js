var inmobiliaria, inmueble, codigoInmueble, dis, varBI = "";
var localLatitud = 4.7055976;
var localLongitud = -74.0531896;
var currentLat = 4.7055976;
var currentLng = -74.0531896;
var map = "";
var markers = [];
var markerP = "";
var circle = "";
var infoWindow = "";
var markerCluster = "";
var loadSelectModal = 0;
var radius = 500;
var input = "";
var searchBox = "";
var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    IOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    }
}
$(document).ready(function() {
    $('#rangoMapa').rangeslider();
    //alert(isMobile.IOS());
    //alert(isMobile.Android());
    // new RangeSlider($('#range'), {
    //     size: 1,
    //     borderSize: 0.4,
    //     percentage: 3,
    // });
    if (isMobile.IOS() != null) {
        $('.btn-whatsapp').css('display', 'none');
    }
});

function checkConnection() {
    var networkState = navigator.connection.type;
    if (networkState == 'none') {
        alert('Verifique la conexión a internet');
        navigator.app.exitApp();
    };
}
onLoad();
// checkConnection();
function buscarInmuebles(latitud, longitud) {
    $('#limitStart').val(0);
    $('.preloader').css('display', 'block');
    var pos = "";
    var duracion = 3000;
    if (latitud == 0 && longitud == 0) {
        navigator.geolocation.getCurrentPosition(function(position) {
            alert('Entro getCurrentPosition');
            localLatitud = position.coords.latitude;
            localLongitud = position.coords.longitude;
            currentLat = position.coords.latitude;
            currentLng = position.coords.longitude;
            pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            map.setCenter(pos);
            getInmuebles(map, pos, dis);
        }, function(error) {
            alert('Error al obtener su ubicacion actual, Verifique que el GPS esta Activado!');
            // }
            navigator.vibrate(1000);
            navigator.notification.alert('Error al obtener su ubicacion actual, Verifique que el GPS esta Activado!', function() {navigator.app.exitApp()}, 'Error en la ubicacion')
        }, {
            maximumAge: 3000,
            timeout: 10000
        });
    } else {
        duracion = 500;
        localLatitud = latitud;
        localLongitud = longitud;
        pos = {
            lat: latitud,
            lng: longitud
        };
        map.setCenter(pos);
        getInmuebles(map, pos, dis);
    }
}

function getInmuebles(map, pos, dis) {
    if ($('#codInm').val() == "") {
        var markerPrimary = new google.maps.Marker({
            position: pos,
            draggable: true,
            map: map,
            icon: "https://www.simiinmobiliarias.com/mcomercialweb/fichas_tecnicas/img/iconoappsimi.png",
            zIndex: 99999999999999999999
        });
        map.setCenter(pos);
        markerP = markerPrimary;
        markerPrimary.setMap(map);
        markerPrimary.addListener('dragend', changeLocation);
        circle = new google.maps.Circle({
            strokeColor: '#FFFFFF',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#979696',
            fillOpacity: 0.35,
            map: map,
            center: pos,
            radius: radius
        });
    }
    var codInm = $('#codInm').val();
    var tipoinm = $('#tipoInmueble').val();
    var gestion = $('#gestion').val();
    var destinacion = $('#destinacion').val();
    var preciofrom = $('#preciofrom').val();
    var precioto = $('#precioto').val();
    var areafrom = $('#areafrom').val();
    var areato = $('#areato').val();
    var banios = $('#banios').val();
    var alcobas = $('#alcobas').val();
    var parqueadero = $('#parqueadero').val();
    $.ajax({
        url: 'https://www.simiinmobiliarias.com/models/appPublica.php?data=1',
        type: 'POST',
        dataType: 'json',
        data: {
            lat: pos.lat,
            lng: pos.lng,
            dist: dis,
            tipoinm: tipoinm,
            gestion: gestion,
            destinacion: destinacion,
            banios: banios,
            alcobas: alcobas,
            parqueadero: parqueadero,
            preciofrom: preciofrom,
            precioto: precioto,
            areafrom: areafrom,
            areato: areato,
            red: 1,
            codInm: codInm
        },
        // beforeSend: function (e) {
        //     $('#loadingmap').html('<img src="' + base_url + 'assets/img/cargando.gif" width="80" />');
        // },
        success: function(data) {
            // if (data.status == 0) {
            //     alert(data.msn);
            //     return false;
            // }
            $('#loadingmap').html('');
            var geocoder = new google.maps.Geocoder;
            // getBarrio(geocoder, map, pos);
            console.log(pos);
            infoWindow = new google.maps.InfoWindow();
            //                    infoWindow.setPosition(pos);
            var marker, i;
            console.log(data);
            var iconCounter = 0;
            if (data.length > 0) {
                if ($('#codInm').val() != "") {
                    localLatitud = parseFloat(data[0].latitud_sit);
                    localLongitud = parseFloat(data[0].longitud_sit);
                    var pos = {
                        lat: parseFloat(data[0].latitud_sit),
                        lng: parseFloat(data[0].longitud_sit)
                    };
                    map.setCenter(pos);
                }
                for (var i in data) {
                    var currentInmu = explode(data[i].idinm, '-');
                    var btnHerraInm = '<div class="optHerramientasInm items inmuebleCall" data-toggle="modal" data-target="#modalCallcenter" data-inmueble="' + data[i].idinm + '"><i class="fa fa-user-circle-o" aria-hidden="true"></i></div>';
                    var btnHerraInmMap = '<button class="btn btn-info gestionarInm inmuebleCall" data-inmueble="' + data[i].idinm + '" data-toggle="modal" data-target="#modalCallcenter"><i class="fa fa-user-circle-o" aria-hidden="true"></i></button>';
                    var styleParent = "";
                    var styleChildren = "";
                    var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                    marker = new google.maps.Marker({
                        position: {
                            lat: parseFloat(data[i].latitud_sit),
                            lng: parseFloat(data[i].longitud_sit)
                        },
                        map: map,
                        icon: data[i].icon
                    });
                    markers.push(marker);
                    var mensaje = data[i].tipoinmueble + ' En ' + data[i].gestion;
                    var url = data[i].urlWhatsapp;
                    var sujeto = 'Simi Inmueble';
                    var fotoSh = data[i].foto;
                    var content = '<div class="fotoInfoWindow displayinline data-inm" style="cursor:pointer" data-inm="' + data[i].idinm + '">\
                                          <div style="display:table;height:100%;width:100%">\
                                            <div style="display:table-cell;vertical-align:middle;width:100%;background-image: url(' + "'" + data[i].foto + "'" + ');background-size: contain;background-position: center center;background-repeat: no-repeat;">\
                                              <!-- <img alt="" src="' + data[i].foto + '"/>-->\
                                            </div>\
                                          </div>\
                                        </div>\
                                        <div class="infoGeneral displayinline" style="cursor:pointer">\
                                          <p class="truncate">' + data[i].tipoinmueble + ' En ' + data[i].gestion + '\
                                          </p>\
                                          <p class="truncate"><i class="fa fa-map-marker"></i> ' + data[i].ciudad + ' - ' + data[i].barrio + '\
                                          </p>\
                                          <div class="propiedades">\
                                            <ul>\
                                                <li>' + data[i].alcobas + ' <i class="fa fa-bed"></i></li>\
                                                <li style="border-left: 1px solid #e0e0e0; border-right: 1px solid #e0e0e0;">' + data[i].banos + ' <i class="fa fa-bath"></i></li>\
                                                <li>' + data[i].area + ' m<sup>2</sup> <i class="flaticon-tools"></i> <small>Aprox.</small></li>\
                                            </ul>\
                                          </div>\
                                        </div>\
                                        <div class="infoDes displayinline">\
                                          <p style="cursor:pointer" class="truncate">\
                                            <b>$ ' + data[i].precio + '</b>\
                                          </p>\
                                          <i aria-hidden="true" class="fa fa-share-alt fa-2x" onclick="window.plugins.socialsharing.share(' + "'" + mensaje + "'" + ', ' + "'" + sujeto + "'" + ', ' + "'" + fotoSh + "'" + ', ' + "'" + url + "'" + ');" style="margin-top:3px"></i>\
                                        </div>\
                                        <div class="closeInfoWindow" style="cursor:pointer">\
                                          <i aria-hidden="true" class="fa fa-times"></i>\
                                        </div>';
                    // console.log(data);
                    google.maps.event.addListener(marker, 'click', (function(marker, content, infoWindow) {
                        return function() {
                            $('.detailInmueble').css('bottom', '-110px');
                            $('.detailInmueble').html(content);
                            $('.preloadDetail').show();
                            setTimeout(function() {
                                $('.preloadDetail').hide();
                                $('.detailInmueble').animate({
                                    bottom: 0
                                }, 500);
                            }, 1000);
                            // infoWindow.setContent(content);
                            // infoWindow.open(map, marker);
                        }
                    })(marker, content, infoWindow));
                    // var markerCluster = new MarkerClusterer(map, markers,
                    //     {imagePath: 'https://www.simiinmobiliarias.com/mcomercialweb/fichas_tecnicas/img/m'});
                    google.maps.event.addListener(map, 'dragend', (function(map, i) {
                        $('#currentPosition').css('color', 'inherit');
                        infoWindow.close();
                    }));
                    google.maps.event.addDomListener(window, "resize", function() {
                        map.setCenter(pos);
                    });
                }
                markerCluster = new MarkerClusterer(map, markers, {
                    imagePath: 'https://www.simiinmobiliarias.com/mcomercialweb/fichas_tecnicas/img/m'
                });
                listadoInmueblesAjax(0);
            }else{
                $('#listado').html('');
                Materialize.toast('No existen resultados!', 3000);
            }
            $('.preloader').css('display', 'none');
        }
    });
}

function listadoInmueblesAjax(loadAjax) {
    var limitStart = $('#limitStart').val();
    var htmlInmueble = "";
    var tipoinm = $('#tipoInmueble').val();
    var gestion = $('#gestion').val();
    var destinacion = $('#destinacion').val();
    var preciofrom = $('#preciofrom').val();
    var precioto = $('#precioto').val();
    var areafrom = $('#areafrom').val();
    var areato = $('#areato').val();
    var banios = $('#banios').val();
    var alcobas = $('#alcobas').val();
    var parqueadero = $('#parqueadero').val();
    var red = $('#viewRed').val();
    var codInm = $('#codInm').val();
    if (loadAjax == 1) {
        $('.preloadDetail').css('display', 'block');
    }
    var listadoInmuebles = ajaxSimple('https://www.simiinmobiliarias.com/models/appPublica.php?data=1', 'POST', 'json', {
        lat: localLatitud,
        lng: localLongitud,
        dist: dis,
        tipoinm: tipoinm,
        gestion: gestion,
        destinacion: destinacion,
        banios: banios,
        alcobas: alcobas,
        parqueadero: parqueadero,
        preciofrom: preciofrom,
        precioto: precioto,
        areafrom: areafrom,
        areato: areato,
        red: 1,
        list: 1,
        limitStart: limitStart,
        limitEnd: 10,
        codInm: codInm
    }, 0);
    // setTimeout(function() {
    listadoInmuebles.success(function(inmuebles) {
        $('.preloadDetail').css('display', 'none');
        if (inmuebles.length > 0) {
            $('#limitStart').val(parseInt(limitStart) + 10);
            for (inm in inmuebles) {
                var mensaje = inmuebles[inm].tipoinmueble + ' En ' + inmuebles[inm].gestion;
                var url = inmuebles[inm].urlWhatsapp;
                var sujeto = 'Simi Inmueble';
                var fotoSh = inmuebles[inm].foto;
                var content2 = '<div class="fotoInfoWindow displayinline data-inm" style="cursor:pointer" data-inm="' + inmuebles[inm].idinm + '">\
                                      <div style="display:table;height:100%;width:100%">\
                                        <div class="imgListadoInm" data-estado="0" data-src="' + inmuebles[inm].foto + '" style="display:table-cell;vertical-align:middle;width:100%;background-image: url(' + "'" + inmuebles[inm].foto + "'" + ');background-size: contain;background-position: center center;background-repeat: no-repeat;">\
                                          <!-- <img alt="" src="' + inmuebles[inm].foto + '"/>-->\
                                        </div>\
                                      </div>\
                                    </div>\
                                    <div class="infoGeneral displayinline" style="cursor:pointer">\
                                      <p class="truncate">' + inmuebles[inm].tipoinmueble + ' En ' + inmuebles[inm].gestion + '\
                                      </p>\
                                      <p class="truncate"><i class="fa fa-map-marker"></i> ' + inmuebles[inm].ciudad + ' - ' + inmuebles[inm].barrio + '\
                                      </p>\
                                      <div class="propiedades">\
                                        <ul>\
                                            <li>' + inmuebles[inm].alcobas + ' <i class="fa fa-bed"></i></li>\
                                            <li style="border-left: 1px solid gray; border-right: 1px solid gray;">' + inmuebles[inm].banos + ' <i class="fa fa-bath"></i></li>\
                                            <li>' + inmuebles[inm].area + ' m<sup>2</sup> <i class="flaticon-tools"></i> <small>Aprox.</small></li>\
                                        </ul>\
                                      </div>\
                                    </div>\
                                    <div class="infoDes displayinline">\
                                      <p style="cursor:pointer" class="truncate">\
                                        <b>$ ' + inmuebles[inm].precio + '</b>\
                                      </p>\
                                      <i aria-hidden="true" class="fa fa-share-alt fa-2x" onclick="window.plugins.socialsharing.share(' + "'" + mensaje + "'" + ', ' + "'" + sujeto + "'" + ', ' + "'" + fotoSh + "'" + ', ' + "'" + url + "'" + ');" style="margin-top:3px"></i>\
                                    </div>\
                                    <div class="closeInfoWindow" style="cursor:pointer">\
                                      <i aria-hidden="true" class="fa fa-times"></i>\
                                    </div>';
                htmlInmueble += '<div class="listInmueble">' + content2 + '</div>';
            };
        } else {
            Materialize.toast('No existen resultados!', 1000, 'rounded')
        }
        if (loadAjax == 0) {
            $('#listado').html(htmlInmueble);
        } else {
            $('#listado').append(htmlInmueble);
        }
    });
    // }, 1000);
}

function loadAsincronoImagenes() {
    // var scrollTop = $('#listado').height();
    // $('.listInmueble').each(function(index, el) {
    //     var item = $(this);
    //     var itemImg = item.find('.imgListadoInm');
    //     var img = itemImg.data('src');
    //     var estado = itemImg.attr('data-estado');
    //     var itemScroll = $(this).offset().top;
    //     if (estado == 0) {
    //         // item.find('.preloader').css('display', 'block');
    //         if (itemScroll <= scrollTop) {
    //             itemImg.css('background-image', 'url(' + img + ')');
    //             itemImg.attr('data-estado', 1);
    //             // itemImg.load(function() {
    //             //     item.find('.preloader').css('display', 'none');
    //             // });
    //         }
    //     }
    // });
    $('#listado').scroll(function(event) {
        if (Math.ceil($(this).scrollTop() + $(this).innerHeight()) >= $(this)[0].scrollHeight) {
            listadoInmueblesAjax(1);
        }
        // var scrollTop = parseInt($('#listado').scrollTop()) + 300;
        // $('.listInmueble').each(function(index, el) {
        //     if (estado == 0) {
        //         var item = $(this);
        //         var itemImg = item.find('.imgListadoInm');
        //         var img = itemImg.data('src');
        //         var estado = itemImg.attr('data-estado');
        //         var itemScroll = $(this).offset().top;
        //         // item.find('.preloader').css('display', 'block');
        //         if (itemScroll <= scrollTop) {
        //             itemImg.css('background-image', 'url(' + img + ')');
        //             itemImg.attr('data-estado', 1);
        //             // itemImg.load(function() {
        //             //     item.find('.preloader').css('display', 'none');
        //             // });
        //         }
        //     }
        // });
    });
}

function deleteMarkers() {
    clearMarkers();
    markers = [];
}

function clearMarkers() {
    setMapOnAll(null);
}

function setMapOnAll(map) {
    console.log(markerCluster);
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(map);
        // markerCluster[i].setMap(map);
        markerCluster.clearMarkers();
    }
    markerP.setMap(map);
    circle.setMap(null);
}

function changeLocation(marker) {
    $('#currentPosition').css('color', 'inherit');
    deleteMarkers();
    // var markerLatLng = marker.getPosition();
    // alert('latitud = ' + this.getPosition().lat() + ' longitud = ' + this.getPosition().lng());
    buscarInmuebles(this.getPosition().lat(), this.getPosition().lng());
}
$('.button-collapse').sideNav();
var prueba = $('#tabs-swipe-demo').tabs({
    'swipeable': true
});

function closeInfoWindow() {
    $(document).on('click', '.closeInfoWindow', function() {
        $('.detailInmueble').css('bottom', '-110px');
    });
}

function openModalFiltro() {
    $('.openModalFiltro').on('click', function() {
        $('#viewFiltro').val(1);
        $('select').material_select();
        $('#filtroMapa').modal();
        $('.numberFormat').mask('000.000.000.000.000', {
            reverse: true
        });
    });
}

function aplicarFiltro() {
    $('.aplicarFiltro').on('click', function(e) {
        e.preventDefault();
        deleteMarkers();
        $('#viewFiltro').val(0);
        $('#filtroMapa').modal('close');
        buscarInmuebles(localLatitud, localLongitud);
    });
}

function viewDetailFull() {
    $(document).on('click', '.fotoInfoWindow,.infoGeneral,.infoDes p', function() {
        // alert();
        var ipUser = "";
        var idinm = $(this).parent().find('.data-inm').data('inm');
        if (idinm == null) {
            idinm = $(this).parent().parent().find('.data-inm').data('inm');
        }
        $.get("http://ipinfo.io", function(response) {
            ipUser = response.ip;
            // alert(ipUser);
            var log = ajaxSimple('https://www.simiinmobiliarias.com/models/appPublica.php?data=6', 'POST', 'json', {
                idinm: idinm,
                ipUser: ipUser
            }, 0);
            // alert(log);
            // console.log(JSON.stringify(log));
            // console.log('Aqui loggggggg');
            log.success(function(data) {
                // alert(data);
                console.log(data);
            });
        }, "jsonp");
        $('.detailInmuebleFull').scrollTop(0);
        $('#viewDetailInmuebleFul').val(1);
        $('.carousel.carousel-slider').removeClass('initialized');
        $('.preloader').css('display', 'block');
        $('#codInmContact').val(idinm);
        var datos = ajaxSimple('https://www.simiinmobiliarias.com/models/callCenter.php?data=3', 'POST', 'json', {
            idinm: idinm
        }, 0);
        var htmlFotos = "";
        datos.success(function(data) {
            console.log(data);
            var msgContacto = "Cliente interesado en " + data.Tipo_Inmueble + " " + idinm + " de " + data.barrio;
            $('#headerContacto').val(msgContacto);
            $('.logoInmo').css('background-image', ' url(' + data.logo + ')');
            $('.tipinm').text(data.Tipo_Inmueble);
            $('.alcobas').text(data.alcobas);
            $('.banios').text(data.banos);
            $('.estrato').text(data.Estrato);
            $('.precio').text('$ ' + data.precio);
            $('.iva').text('$ ' + data.ValorIva);
            $('.admin').text('$ ' + data.Administracion);
            $('.ciudad').text(data.ciudad);
            $('.zona').text(data.zona);
            $('.localidad').text(data.localidad);
            $('.barrio').text(data.barrio);
            $('.gestion').text(data.Gestion);
            $('.area').text(data.AreaLote);
            $('.garaje').text(data.garaje);
            $('.descripcion').text(data.descripcionlarga);
            for (i in data.fotos) {
                htmlFotos += '<div class="carousel-item" style="background-image: url(' + "'" + data.fotos[i].foto + "'" + ');background-size: contain;background-position: center center;background-repeat: no-repeat;"></div>';
            }
            var prevnext = '<div class="prev">\
                                    <div class="icon">\
                                        <div class="icono">\
                                            <i class="fa fa-chevron-left" aria-hidden="true"></i>\
                                        </div>\
                                    </div>\
                                </div>\
                                <div class="next">\
                                    <div class="icon">\
                                        <div class="icono">\
                                            <i class="fa fa-chevron-right" aria-hidden="true"></i>\
                                        </div>\
                                    </div>\
                                </div>';
            $('.carousel.carousel-slider').html(htmlFotos);
            // $('.carousel.carousel-slider').append(prevnext);
            if (data.asesor.length > 0 && data.asesor[0].celular != "") {
                $('.call').attr('data-tel', 'tel:' + data.asesor[0].celular);
                // $('.call').attr('onclick', 'window.plugins.CallNumber.callNumber(onSuccess, onError,' + data.asesor[0].celular + ', false);');
            } else {
                $('.call').attr('data-tel', 'tel:' + data.inmobiliaria.telefono);
                // $('.call').attr('onclick', 'window.plugins.CallNumber.callNumber(onSuccess, onError,' + data.inmobiliaria.telefono + ', false);');
            }
            if (data.asesor.length == 0 && data.inmobiliaria.telefono == "") {
                $('.call').prop('disabled', true);
            } else {
                $('.call').prop('disabled', false);
            }
            // alert(data.asesor);
            // console.log(data.asesor[0]);
            // alert(data.asesor.length);
            if (data.asesor.length > 0) {
                if (data.asesor[0].celular.length == 10) {
                    // alert();
                    $('.btn-whatsapp').prop('disabled', false);
                    $('.btn-whatsapp').attr('data-nW', data.asesor[0].celular);
                } else {
                    $('.btn-whatsapp').prop('disabled', true);
                }
            } else {
                $('.btn-whatsapp').prop('disabled', true);
            }
            // $('#pruebaNumero').val(data.asesor[0].celular);
            // console.log(data.asesor[0].celular);
            // $('#pruebaNumeroInmo').val(data.inmobiliaria.telefono);
            /*
                 Llenar Tabla Caracteristicas Internas
                 */
            var filasCaracInter = Math.ceil(data.caracteristicasInternas.length / 2);
            var columnas = 2;
            var item = 0;
            var tdcarac = '';
            for (var t = 0; t < filasCaracInter; t++) {
                tdcarac = tdcarac + '<tr>';
                for (var y = 0; y < columnas; y++) {
                    if (data.caracteristicasInternas[item]) {
                        tdcarac = tdcarac + "<td class=''>" + data.caracteristicasInternas[item].icono_car + " " + data.caracteristicasInternas[item].Descripcion + "</td>";
                    } else {
                        tdcarac = tdcarac + "<td></td>";
                    }
                    ++item;
                }
                tdcarac = tdcarac + "</tr>";
            }
            $('.caracteristicasInternas').html(tdcarac);
            /*
                 Fin Caracteristicas Internas
                 */
            /*
                 Llenar Tabla Caracteristicas Externas
                 */
            var filasCaracExter = Math.ceil(data.caracteristicasExternas.length / 2);
            var item2 = 0;
            var tdcarac2 = '';
            for (var t2 = 0; t2 < filasCaracExter; t2++) {
                tdcarac2 = tdcarac2 + '<tr>';
                for (var y2 = 0; y2 < columnas; y2++) {
                    if (data.caracteristicasExternas[item2]) {
                        tdcarac2 = tdcarac2 + "<td class=''>" + data.caracteristicasExternas[item2].icono_car + " " + data.caracteristicasExternas[item2].Descripcion + "</td>";
                    } else {
                        tdcarac2 = tdcarac2 + "<td></td>";
                    }
                    ++item2;
                }
                tdcarac2 = tdcarac2 + "</tr>";
            }
            $('.caracteristicasExternas').html(tdcarac2);
            /*
                 Fin Caracteristicas Externas
                 */
            /*
                 Llenar Tabla Caracteristicas Alrededores
                 */
            var filasCaracAlrede = Math.ceil(data.caracteristicasAlrededores.length / 2);
            var item3 = 0;
            var tdcarac3 = '';
            for (var t3 = 0; t3 < filasCaracAlrede; t3++) {
                tdcarac3 = tdcarac3 + '<tr>';
                for (var y3 = 0; y3 < columnas; y3++) {
                    if (data.caracteristicasAlrededores[item3]) {
                        tdcarac3 = tdcarac3 + "<td class=''>" + data.caracteristicasAlrededores[item3].icono_car + " " + data.caracteristicasAlrededores[item3].Descripcion + "</td>";
                    } else {
                        tdcarac3 = tdcarac3 + "<td></td>";
                    }
                    ++item3;
                }
                tdcarac3 = tdcarac3 + "</tr>";
            }
            $('.caracteristicasAlrededores').html(tdcarac3);
            /*
                 Fin Caracteristicas Alrededores
                 */
            setTimeout(function() {
                $('.preloader').css('display', 'none');
                $('.detailInmuebleFull').animate({
                    right: 0
                }, 500);
                $('.carousel.carousel-slider').carousel({
                    fullWidth: true,
                    duration: 50
                });
                setTimeout(function() {
                    $('.optDetailInmueble').animate({
                        bottom: 0
                    }, 500);
                }, 500);
            }, 1000);
        });
        if (!$('.general').hasClass('active')) {
            $('.general').trigger('click');
        }
    });
}

function callNumber() {
    $(document).on('click', '.call', function(e) {
        // document.location.href = $(this).attr('data-tel');
        cordova.InAppBrowser.open($(this).attr('data-tel'), '_system');
    });
}

function closeDetailFull() {
    $('.closeDetailFull').on('click', function() {
        $('#viewDetailInmuebleFul').val(0);
        $('.optDetailInmueble').css({
            bottom: '-100%'
        });
        $('.detailInmuebleFull').animate({
            right: '-100%'
        }, 500);
    });
}

function openModalContacto() {
    $('.btn-contacto').on('click', function() {
        $('#contacto').modal();
        $('#viewContacto').val(1);
    });
}

function sendContacto() {
    var formContacto = $('form[name=contacto]');
    validateParsley(formContacto);
    formContacto.parsley();
    formContacto.on('submit', function(e) {
        e.preventDefault();
        if ($(this).parsley().validate() === true) {
            $('form[name=contacto] button').prop('disabled', true);
            $('.preloader').css('display', 'block');
            var dataForm = $(this).serialize();
            var sendEmail = ajaxSimple('https://www.simiinmobiliarias.com/models/appPublica.php?data=5', 'post', 'json', dataForm, 0);
            sendEmail.success(function(data) {
                $('form[name=contacto] button').prop('disabled', false);
                $('.preloader').css('display', 'none');
                if (data.status == 'Ok') {
                    swal("", data.msg, "success");
                } else {
                    swal("", data.msg, "error");
                }
                $('#contacto').modal('close');
                $('#viewContacto').val(0);
            });
        }
    });
}

function viewPlaces() {
    $('.viewPlaces').on('click', function(e) {
        $('#viewPlaces').val(1);
        loadPlacesMap();
        if (!$('ul.tabs li.tab a[href="#mapa"]').hasClass('active')) {
            $('ul.tabs li.tab a[href="#mapa"]').trigger('click');
        }
        $('.navBarPrimary').css('display', 'none');
        $('.navBarPlaces').css('display', 'block');
        $('.places').animate({
            bottom: 0
        }, 500);
    });
}

function closePlaces() {
    $('.closePlaces').on('click', function() {
        $('#viewPlaces').val(0);
        $('.navBarPlaces').css('display', 'none');
        $('.navBarPrimary').css('display', 'block');
        $('.places').animate({
            bottom: "-100%"
        }, 500);
        deleteMarkers();
        buscarInmuebles(localLatitud, localLongitud);
    });
}

function selectPlaces() {
    $(document).on('click', '.places ul li', function() {
        $('.places ul li').removeClass('active');
        $(this).addClass('active');
        loadPlacesMap();
    });
}

function loadPlacesGroup() {
    var places = ajaxSimple('https://www.simiinmobiliarias.com/models/appPublica.php?data=4', 'post', 'json', {}, 0);
    var htmlPlaces = "";
    var active = "active";
    places.success(function(data) {
        console.log(data);
        console.log('Aquiuiuiiiiuiuiu');
        for (i in data) {
            console.log(data[i].icon);
            if (i != 0) {
                active = "";
            }
            htmlPlaces = '<li data-id="' + data[i].id + '" class="waves-effect waves-light ' + active + '">' + data[i].icon + '</li>';
            $('.places ul').append(htmlPlaces);
        }
        var wItem = 60;
        var tItems = $('.places ul li').length;
        var tWP = parseInt(tItems) * wItem;
        var wW = $(window).width();
        if (tWP < wW) {
            $('.places ul').css({
                width: tWP + 'px',
                margin: '0 auto'
            });
            // $('.places ul').css('width', '100%');
        } else if (tWP == wW) {
            $('.places ul').css('width', '100%');
        } else {
            $('.places ul').css('width', parseInt(tWP) + 60 + 'px');
        }
        console.log(htmlPlaces);
        // setTimeout(function(){
        //     $('.tooltipped').tooltip({delay: 50});
        // },2000);
    });
}

function loadPlacesMap() {
    deleteMarkers();
    var markerPrimary = new google.maps.Marker({
        position: {
            lat: localLatitud,
            lng: localLongitud
        },
        draggable: false,
        map: map,
        icon: "https://www.simiinmobiliarias.com/mcomercialweb/fichas_tecnicas/img/iconoappsimi.png",
        zIndex: 99999999999999999999
    });
    map.setCenter({
        lat: localLatitud,
        lng: localLongitud
    });
    markerP = markerPrimary;
    markerPrimary.setMap(map);
    // markerPrimary.addListener('dragend', changeLocation);
    circle = new google.maps.Circle({
        strokeColor: '#FFFFFF',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#979696',
        fillOpacity: 0.35,
        map: map,
        center: {
            lat: localLatitud,
            lng: localLongitud
        },
        radius: radius
    });
    var tiposit = $('.places ul li.active').data('id');;
    var sitios = ajaxSimple('https://www.simiinmobiliarias.com/models/appPublica.php?data=3', 'POST', 'json', {
        tiposit: tiposit,
        lat: localLatitud,
        lng: localLongitud,
        dis: dis
    }, 0);
    sitios.success(function(data) {
        if (data.length > 0) {
            for (var i in data) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(data[i].latitud_sit, data[i].longitud_sit),
                    map: map,
                    icon: data[i].icon
                });
                markers.push(marker);
                var infoSit = "";
                var telefonoSit = "";
                if (data[i].info_sit.length > 17) {
                    infoSit = data[i].info_sit + '<br>';
                }
                if (data[i].telefono_sit.length > 22) {
                    telefonoSit = ' </br>' + data[i].telefono_sit;
                }
                var contentSitio = data[i].tipo_sit + '<br>' + data[i].label_sit + '<br>' + infoSit + data[i].direccion_sit + telefonoSit;
                google.maps.event.addListener(marker, 'click', (function(marker, contentSitio, infoWindow) {
                    return function() {
                        infoWindow.setContent(contentSitio);
                        infoWindow.open(map, marker);
                    }
                })(marker, contentSitio, infoWindow));
            }
        } else {
            Materialize.toast('No existen resultados!', 1000, 'rounded');
        }
    })
};

function rangoMapa() {
    $('.rangoMapa ul li').on('click', function(e) {
        $('.rangoMapa ul li').removeClass('active');
        $(this).addClass('active');
        deleteMarkers();
        dis = $(this).data('km');
        if (dis == '0.5') {
            radius = 500;
        } else {
            radius = parseInt($(this).data('km') + '000');
        }
        if ($('#viewPlaces').val() == 1) {
            loadPlacesMap();
        } else {
            buscarInmuebles(localLatitud, localLongitud);
        }
    });
}

function prevCarousel() {
    $(document).on('click', '.prev', function() {
        $('.carousel').carousel('prev');
    });
}

function nextCarousel() {
    $(document).on('click', '.next', function() {
        $('.carousel').carousel('next');
    });
}

function onLoad() {
    // alert('deviceready');
    document.addEventListener("deviceready", onDeviceReady, false);
}
// PhoneGap is loaded and it is now safe to call PhoneGap methods
//
function onDeviceReady() {
    dis = 0.5;
    $('.preloader').css('display', 'block');
    // alert($(window).width());
    // alert($(window).height());
    map = new google.maps.Map(document.getElementById('map-filter'), {
        center: {
            lat: localLatitud,
            lng: localLongitud
        },
        zoom: 15,
        mapTypeControl: false,
        streetViewControl: false,
        zoomControl: true,
        zoomControlOptions: {
            position: google.maps.ControlPosition.TOP_RIGHT
        },
    });
    input = document.getElementById('pac-input');
    currentPosition = document.getElementById('currentPosition');
    var options = {
        componentRestrictions: {
            country: "co"
        }
    };
    autocomplete = new google.maps.places.Autocomplete(input, options);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
    map.controls[google.maps.ControlPosition.RIGHT_CENTER].push(currentPosition);
    setTimeout(function() {
        input.style.display = 'block';
        currentPosition.style.display = 'block';
    }, 1000);
    google.maps.event.addDomListener(currentPosition, 'click', function() {
        // map.setCenter({lat:localLatitud,lng:localLongitud});
        $(this).css('color', '#2979ff');
        deleteMarkers();
        map.setCenter({
            lat: currentLat,
            lng: currentLng
        });
        map.setZoom(15);
        buscarInmuebles(0, 0);
    });
    // map.addListener('bounds_changed', function() {
    //     searchBox.setBounds(map.getBounds());
    // });
    autocomplete.addListener('place_changed', function() {
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            window.alert("No existen resultados con la busqueda: '" + place.name + "'");
            return;
        }
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(15); // Why 17? Because it looks good.
        }
        console.log(place.geometry.location.lat());
        console.log(place.geometry.location.lng());
        console.log('Aquisiodasjkfhkjashdfjaskdhfj');
        map.setZoom(15);
        deleteMarkers();
        buscarInmuebles(place.geometry.location.lat(), place.geometry.location.lng());
        setTimeout(function() {
            map.setCenter({
                lat: localLatitud,
                lng: localLongitud
            });
        }, 1000);
        // places.forEach(function(place) {
        //     // console.log(place);
        //     if (!place.geometry) {
        //         console.log("Returned place contains no geometry");
        //         return;
        //     }
        //     map.setZoom(15); 
        //     deleteMarkers();
        //     buscarInmuebles(place.geometry.location.lat(),place.geometry.location.lng());
        //     setTimeout(function(){
        //         map.setCenter({lat: localLatitud,lng:localLongitud});
        //     },1000);
        //     // var icon = {
        //     //     url: place.icon,
        //     //     size: new google.maps.Size(71, 71),
        //     //     origin: new google.maps.Point(0, 0),
        //     //     anchor: new google.maps.Point(17, 34),
        //     //     scaledSize: new google.maps.Size(25, 25)
        //     // };
        //     // // Create a marker for each place.
        //     // markers.push(new google.maps.Marker({
        //     //     map: map,
        //     //     icon: icon,
        //     //     title: place.name,
        //     //     position: place.geometry.location
        //     // }));
        //     if (place.geometry.viewport) {
        //         // Only geocodes have viewport.
        //         bounds.union(place.geometry.viewport);
        //     }
        // });
        // map.fitBounds(bounds);
    });
    if (navigator.geolocation) {
        buscarInmuebles(0, 0);
    } else {
        alert('Debe tener activado la Geolocalización en su navegador!');
    }
    loadPlacesGroup();
    closePlaces();
    selectPlaces();
    viewPlaces();
    sendContacto();
    openModalContacto();
    closeDetailFull();
    viewDetailFull();
    aplicarFiltro();
    openModalFiltro();
    closeInfoWindow();
    loadAsincronoImagenes();
    rangoMapa();
    sendWhatsapp();
    prevCarousel();
    nextCarousel();
    callNumber();
    // currentPosition();
    // setInterval(function(){
    //   $('.carousel').carousel('next');
    // },5000);
    // Llenar Selects Filtro Mapa
    setTimeout(function() {
        getSelectSelectedNew('tipoinmuebles', 'idTipoInmueble', 'Descripcion', '', '', '', '', 0, 'json', '#tipoInmueble', 'Tipo de inmueble', 0, 0);
        getSelectSelectedNew('gestioncomer', 'IdGestion', 'NombresGestion', "", '', '', "order by NombresGestion", 0, 'json', "#gestion", "Seleccione Gestión", 0, 0);
    }, 2000);
    // Register the event listener
    // alert('onDeviceReady');
    document.addEventListener("backbutton", onBackKeyDown, false);
    document.addEventListener("offline", onOffline, false);
}

function onOffline() {
    alert('Se ah perdido la conexion.');
    navigator.app.exitApp();
}
// Handle the back button
//
function onBackKeyDown() {
    var viewDetailFull = $('#viewDetailInmuebleFul').val();
    var viewContacto = $('#viewContacto').val();
    var viewPlaces = $('#viewPlaces').val();
    var viewFiltro = $('#viewFiltro').val();
    if (viewContacto == 1) {
        $('#contacto').modal('close');
        $('#viewContacto').val(0);
        // console.log('viewContacto');
    } else if (viewDetailFull == 1) {
        $('.optDetailInmueble').css({
            bottom: '-100%'
        });
        $('.detailInmuebleFull').animate({
            right: '-100%'
        }, 500);
        $('#viewDetailInmuebleFul').val(0);
        // console.log('viewDetailInmuebleFul');
    } else if (viewPlaces == 1) {
        $('#viewPlaces').val(0);
        $('.navBarPlaces').css('display', 'none');
        $('.navBarPrimary').css('display', 'block');
        $('.places').animate({
            bottom: "-100%"
        }, 500);
        deleteMarkers();
        buscarInmuebles(localLatitud, localLongitud);
        // console.log('viewPlaces');
    } else if (viewFiltro == 1) {
        $('#viewFiltro').val(0);
        $('#filtroMapa').modal('close');
        // console.log('viewFiltro');
    } else {
        if (confirm('Desea salir de la aplicacion?')) {
            navigator.app.exitApp();
        }
    }
}

function sendWhatsapp() {
    $(document).on('click', '.btn-whatsapp', function() {
        var num = $(this).attr('data-nW');
        var opciones = new ContactFindOptions();
        opciones.filter = num;
        var fields = ["name", "displayName", "emails", "phoneNumbers"];
        navigator.contacts.find(fields, function contactSuccess(contacts) {
            // console.log(JSON.stringify(contacts));
            // alert(JSON.stringify(contacts));
            if (contacts.length > 0) {
                cordova.plugins.Whatsapp.send(num);
            } else {
                var nombreContacto = prompt('Debemos agregar el contacto a tu lista, Por Favor ingresa un nombre');
                if (nombreContacto) {
                    var myContact = navigator.contacts.create();
                    myContact.displayName = nombreContacto;
                    myContact.nickname = nombreContacto;
                    myContact.phoneNumbers = [{
                        "type": "mobile",
                        "value": num,
                        "id": 0,
                        "pref": false
                    }];
                    myContact.save(function() {
                        // alert('Contacto Guardado');
                    }, function() {
                        alert('Error al guardar el contacto');
                    });
                    setTimeout(function() {
                        // alert('Aqui entro!');
                        $(".btn-whatsapp").trigger('click');
                    }, 2000);
                } else {
                    // alert('Cancelo el prompt');
                }
            }
        }, function contactError(contacts) {
            alert('Error buscando contactos');
        }, opciones);
        // alert(num);
        // cordova.plugins.Whatsapp.send(num);
    });
}
// function currentPosition() {
//     $(this).css('color', '#2979ff')
//     buscarInmuebles(0, 0);
// }
// function placeCall(tel) {
//     // console.log(tel);
//     // console.log($(tel).attr('data-tel'));
//     if (window.cordova) {
//         cordova.InAppBrowser.open($(tel).attr('data-tel'), '_system');
//     }
// }
function onSuccess(result) {
    // alert('Success Llamada: ' + result);
    // console.log("Success:"+result);
}

function onError(result) {
    // alert('Error Llamada: ' + result);
    // console.log("Error:"+result);
}